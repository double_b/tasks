DIR=$(dir $(realpath $(firstword $(MAKEFILE_LIST))))

install:
	rm -f /usr/local/bin/tasks
	ln -s ${DIR}bin/tasks /usr/local/bin/tasks
	mkdir -p /work/tasks


examples:
	cp -r examples/* /work/tasks/