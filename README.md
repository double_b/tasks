# Task definitions in pure bash

### Installation
```bash
sudo make install
```

### Install examples

```bash
sudo make examples
```

### Usage

List namespaces:
```bash
$ tasks -l

Namespaces:
aws  docker-machine  local
```


List tasks in the namespace:

```bash
$ tasks -l local

Task list:
ssh_main #ssh to main 
ssh_test #ssh to test
tunnel_main #ssh tunnel to main  80 & 443 & 8443
tunnel_test #ssh tunnel to  test 80 & 443 & 8443

```

Run task:

```bash
$ task -r local ssh_main

Last login: Wed Dec 12 19:22:53 2018 from localhost
[root@example.com ~]# 
```


